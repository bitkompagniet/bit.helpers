﻿jQuery(function($) {

    $.fn.extend({

        asyncSubmit: function(options) {

            return this.each(function () {

                var $form = $(this);

                $form.trigger('begin-async-submit.bit');

                if ($form.hasClass('is-submitting')) return;
                $form.addClass('is-submitting');
                
                var serialized = $form.serialize();
                var url = $form.attr('action');
                var method = $form.attr('method').toUpperCase();

                var settings = $.extend({
                    ajaxSettings: {
                        type: method,
                        url: url,
                        data: serialized,
                        timeout: 100000,
                        dataType: 'json'
                    }
                }, options);

                var unusualTimer = setTimeout(function() {
                    $form.trigger('slow-async-submit.bit');
                }, 5000);

                $.ajax(settings.ajaxSettings).done(function (data) {

                    if (data && data.Status) {
                        $form.trigger(data.Status.toLowerCase() + '-async-submit.bit', data);
                    }

                }).fail(function(xhr, textStatus, errorThrown) {
                    
                    $form.trigger('request-failure-async-submit.bit', {xhr: xhr, status: textStatus, error: errorThrown});

                }).always(function () {

                    clearTimeout(unusualTimer);
                    $form.removeClass('is-submitting');

                    $form.trigger('end-async-submit.bit');
                });

            });

        },

        asyncSubmitSetup: function (options) {

            var defaults = { manual: false };

            var settings = $.extend({}, defaults, options || {});

            return this.each(function (index, item) {

                var $form = $(this);

                if (!settings.manual) {

                    $form.on('submit', function (event) {
                        event.preventDefault();
                        $form.asyncSubmit();
                    });

                }
            });
        }
    });

    $('form[data-async-submit]').asyncSubmitSetup();

});