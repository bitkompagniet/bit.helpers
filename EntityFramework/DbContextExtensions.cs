﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace Bit.Helpers.EntityFramework
{
    public static class DbContextExtensions
    {
        public static IEnumerable<PropertyInfo> EntityFrameworkSimpleProperties(this IEnumerable<PropertyInfo> properties)
        {
            return properties.Where(IsEntityFrameworkSimpleProperty);
        }

        public static IEnumerable<PropertyInfo> EntityFrameworkNavigationProperties<TModel>(this IEnumerable<PropertyInfo> properties)
        {
            return properties.Where(x => x.GetGetMethod().IsVirtual && typeof(TModel).IsAssignableFrom(x.PropertyType.EntityFrameworkPropertyUnderlyingType()));
        }

        public static Type EntityFrameworkPropertyUnderlyingType(this Type type)
        {
            return type.IsGenericType ? type.GetGenericArguments().Single() : type;
        }

        public static IEnumerable<PropertyInfo> CollectionProperties(this IEnumerable<PropertyInfo> properties)
        {
            return properties.Where(x => typeof(IEnumerable).IsAssignableFrom(x.PropertyType));
        }

        public static IEnumerable<PropertyInfo> EntityFrameworkNavigationCollectionProperties<TModel>(this IEnumerable<PropertyInfo> properties)
        {
            return EntityFrameworkNavigationProperties<TModel>(properties).Intersect(CollectionProperties(properties));
        }

        public static IEnumerable<PropertyInfo> EntityFrameworkNavigationSingleProperties<TModel>(this IEnumerable<PropertyInfo> properties)
        {
            return EntityFrameworkNavigationProperties<TModel>(properties).Except(CollectionProperties(properties));
        }

        private static readonly IEnumerable<Type> _entityFrameworkSimpleTypes = new[]
        {
            typeof(string), typeof(decimal), typeof(float), typeof(int), typeof(bool), typeof(double), typeof(byte[]), 
            typeof(Int16), typeof(Int32), typeof(Int64), typeof(SByte),
            typeof(DateTime), typeof(TimeSpan)
        };

        private static bool IsEntityFrameworkSimpleProperty(this PropertyInfo property)
        {
            return _entityFrameworkSimpleTypes.Contains(property.PropertyType);
        }

        public static void RunOnObjectModel(this DbContext context, object model, Action<object> action)
        {
            _objectCache.Clear();
            DoRecursively(model, action);
        }

        private static readonly HashSet<object> _objectCache = new HashSet<object>();

        private static void DoRecursively(object model, Action<object> action)
        {
            if (_objectCache.Contains(model)) return;
            _objectCache.Add(model);

            action(model);

            var allSubmodels =
                EntityFrameworkNavigationSingleProperties<object>(model.GetType().GetProperties()).Select(x => x.GetValue(model))
                    .Union(EntityFrameworkNavigationCollectionProperties<object>(model.GetType().GetProperties()).SelectMany(x => x.GetValue(model) as IEnumerable<object>));

            foreach (var submodel in allSubmodels)
            {
                DoRecursively(submodel, action);
            }
        }
    }
}